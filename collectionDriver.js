
/*
<Manish>
Entities are assigned a unique _id field of datatype ObjectID that MongoDB uses for optimized lookup and insertion. 
Since ObjectID is a BSON type and not a JSON type, you’ll have to convert any incoming strings 
to ObjectIDs if they’re to be used when comparing against an “_id” field.</Manish>
*/
var ObjectID = require('mongodb').ObjectID;


CollectionDriver = function(db) {
  this.db = db; 
 /* <manish>This function defines the CollectionDriver constructor method; 
	    it stores a MongoDB client instance for later use  </manish> */
};


CollectionDriver.prototype.getCollection = function(collectionName, callback) {
  this.db.collection(collectionName, function(error, the_collection) {
    if( error ) callback(error);
    else callback(null, the_collection);
  });
};


CollectionDriver.prototype.findAll = function(collectionName, callback) {
    this.getCollection(collectionName, function(error, the_collection) { //manish : gets the collection
      if( error ) callback(error);
      else {
        the_collection.find().toArray(function(error, results) { // manish : check function pass in call back parameter)
          if( error ) callback(error);
          else callback(null, results);
        });
      }
    });
};



CollectionDriver.prototype.get = function(collectionName, id, callback) { //A
    this.getCollection(collectionName, function(error, the_collection) {
        if (error) callback(error);
        else {
            var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$"); //B
            if (!checkForHexRegExp.test(id)) callback({error: "invalid id"});
            else the_collection.findOne({'_id':ObjectID(id)}, function(error,doc) { //C
                if (error) callback(error);
                else callback(null, doc);
            });
        }
    });
};


//save new object
CollectionDriver.prototype.save = function(collectionName, obj, callback) {
    this.getCollection(collectionName, function(error, the_collection) { //A
      if( error ) callback(error)
      else {
        obj.created_at = new Date(); //B
        the_collection.insert(obj, function() { //C
          callback(null, obj);
        });
      }
    });
};


//update a specific object
CollectionDriver.prototype.update = function(collectionName, obj, entityId, callback) {
    this.getCollection(collectionName, function(error, the_collection) {
        if (error) callback(error);
        else {
            obj._id = ObjectID(entityId); //A convert to a real obj id
            obj.updated_at = new Date(); //B
            the_collection.save(obj, function(error,doc) { //C
                if (error) callback(error);
                else callback(null, obj);
            });
        }
    });
};



//delete a specific object
CollectionDriver.prototype.delete = function(collectionName, entityId, callback) {
    this.getCollection(collectionName, function(error, the_collection) { //A
        if (error) callback(error);
        else {
            the_collection.remove({'_id':ObjectID(entityId)}, function(error,doc) { //B
                if (error) callback(error);
                else callback(null, doc);
            });
        }
    });
};




exports.CollectionDriver = CollectionDriver; // this should be the last line to get exported correctly 
