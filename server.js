#!/bin/sh
//bin/false || `which node || which nodejs` << `tail -n +2 $0`
console.log('ok');

var http = require('http'),
    express = require('express'),
    path = require('path');

var formidable = require('formidable'),
    util = require('util'),
    fs=require('fs-extra');;

var app = express();
app.set('port', process.env.OPENSHIFT_NODEJS_PORT  || process.env.PORT || 3000 || 8080);

app.set('views', path.join(__dirname, 'views')); //A
app.set('view engine', 'jade'); //B

app.use(express.bodyParser());

var MongoClient = require('mongodb').MongoClient,
Server = require('mongodb').Server,
CollectionDriver = require('./collectionDriver').CollectionDriver;


var mongoHost = process.env.OPENSHIFT_NODEJS_IP ;//'localHost'; //A
var mongoPort = 27017;
var collectionDriver;

var connection_string = 'mongodb://'  + 'admin:LLEuuEIxIU6G@' +
'127.12.12.130' + ':27017' +
'/' +
'bugmanish';

if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
  connection_string =  process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
  process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
  process.env.OPENSHIFT_APP_NAME;
  console.error(" string we created is :::  ["+connection_string+"]");
  console.error(" OPENSHIFT_MONGODB_DB_URL  string ["+process.env.OPENSHIFT_MONGODB_DB_URL+"]");
}

var db = process.env.OPENSHIFT_APP_NAME ;
MongoClient.connect('mongodb://'+connection_string, function(err, db) {
		if(err) {
		console.error("throwing an error ");
		throw err;
		}
		else
		{
		console.log("mongo db was able to be set up usign the connection string ["+'mongodb://'+connection_string+"]");
		}
		})

var mongoClient = new MongoClient( new Server ( "127.12.12.130" , 27017 ) ); //Ba

app.use(express.static(path.join(__dirname, 'public'))); /* <== <manish> this will serve all ur static pages in public folder */

/*
Manish
Advanced Routing
Static pages are all well and good, but the real power of Express is found in dynamic routing.

Express uses a regular expression matcher on the route string and allows you to define parameters for the routing.

For example, the route string can contain the following items:
1)static terms ?~@~T /files only matches http://localhost:300/pages
2)parameters prefixed with ?~@~\:?~@~] ?~@~T /files/:filename matches /files/foo and /files/bar, but not /files
3) optional parameters suffixed with ?~@~\??~@~] ?~@~T /files/:filename? matches both ?~@~\/files/foo?~@~] and ?~@~\/files?~@~]
4)regular expressions ?~@~T /^\/people\/(\w+)/ matches /people/jill and /people/john

For example if we write something like :
app.get('/:a?/:b?/:c?', function (req,res) {
                console.log('Express server  function  [78]');
        res.send(req.params.a + ' ' + req.params.b + ' ' + req.params.c);
});
This creates a new route which takes up to three path levels and displays those path components in the response body.
 Anything that starts with a : is mapped to a request parameter of the supplied name.
Restart your Node instance and point your browser to: http://localhost:3000/hi/every/body. You?~@~Yll see that in body of page we get

hi every body

*/

app.get('/:collection', function(req, res) { //A
   console.log('Express server  function  [90]');
   console.log('Express server  function  params['+params+']');
   var params = req.params; //B
   collectionDriver.findAll(req.params.collection, function(error, objs) { 
	   // C <= for above  these are our defined functions& not sucess,failure
	   if (error) { res.send(400, error); } //D
	   else {
	   if (req.accepts('html')) { //E
	   res.render('data',{objects: objs, collection: req.params.collection}); //F

	   var Quiche = require('quiche');

	   var pie = new Quiche('pie');
	   pie.setTransparentBackground(); // Make background transparent
	   pie.addData(3000, 'Foo', 'FF0000');
	   pie.addData(2900, 'Bas', '0000FF');
	   pie.addData(1500, 'Bar', '00FF00');
	   var imageUrl = pie.getUrl(false); // First param controls http vs. https
	   res.render(imageUrl);

	   } else {
	   res.set('Content-Type','application/json'); //G
	   res.send(200, objs); //H
	   }
	   }
   });
});

app.get('/:collection/:entity', function(req, res) { //I
   console.log('Express server  function  [a111]');
   var params = req.params;
   var entity = params.entity;
   var collection = params.collection;
   if (entity) {
       collectionDriver.get(collection, entity, function(error, objs) { //J
          if (error) { res.send(400, error); }
          else { res.send(200, objs); } //K
       });
   } else {
      res.send(400, {error: 'bad url', url: req.url});
   }
});

app.post('/:collection', function(req, res) { //A
    console.log('Express server  function  [a112]');
    var object = req.body;
    var collection = req.params.collection;
    collectionDriver.save(collection, object, function(err,docs) {
          if (err) { res.send(400, err); }
          else { res.send(201, docs); } //B
     });
});


app.put('/:collection/:entity', function(req, res) { //A
    console.log('Express server  function  [a114]');
    var params = req.params;
    var entity = params.entity;

    var collection = params.collection;
    if (entity) {
       collectionDriver.update(collection, req.body, entity, function(error, objs) { //B
          if (error) { res.send(400, error); }
          else { res.send(200, objs); } //C
       });
   } else {
       var error = { "message" : "Cannot PUT a whole collection" };
       res.send(400, error);
   }
});

app.delete('/:collection/:entity', function(req, res) { //A
    console.log('Express server  function  [a115]');
    var params = req.params;
    var entity = params.entity;
    var collection = params.collection;
    if (entity) {
       collectionDriver.delete(collection, entity, function(error, objs) { //B
          if (error) { res.send(400, error); }
          else { res.send(200, objs); } //C 200 b/c includes the original doc
       });
   } else {
       var error = { "message" : "Cannot DELETE a whole collection" };
       res.send(400, error);
   }
});


app.get('/', function (req, res) {
                console.log('i MANISH Express server  function  [a117]');
                console.log('Express server  got / request ');
                res.send('<html><body><h1>Hello World . This is our first page </h1></body></html>');
                });

app.use(function (req,res) {
                console.log('Express server  function  [a118]');
                res.render('404', {url:req.url});
                });

http.createServer(app).listen(8080,'127.12.12.129' ,function(){
                console.log('Express server listening on port ' + app.get('port'));
                });



